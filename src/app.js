const express = require("express");
const mongoose = require("mongoose");
// const dotenv = require("dotenv");

// dotenv.config({ path: `${__dirname}/../config.env` }); //{ path: "../config.env" };

const app = express();

const cardRoute = require("./routes/cardRoute");
const customerRoute = require("./routes/customerRoute");

//middleWares
app.use(express.json());

app.use("/api/user", customerRoute);
app.use("/api/card", cardRoute);

// db connectionS
const DB = process.env.DATABASE || "mongodb://localhost:27017/jaiKisan";
mongoose.connect(DB).then(() => {
  console.log("DB Connected...");
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Express is running on port ${port}`);
});
