const express = require("express");

const router = express.Router();

const {
  createCustomer,
  getAllCustomer,
  deleteCustomer,
} = require("../controller/customerController");

router.route("/").post(createCustomer).get(getAllCustomer);

router.route("/:id").delete(deleteCustomer);

module.exports = router;
