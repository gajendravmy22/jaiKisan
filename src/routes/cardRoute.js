const express = require("express");

const router = express.Router();

const { createCard, ListCard } = require("../controller/cardController");

router.route("/").post(createCard).get(ListCard);

module.exports = router;
