const mongoose = require("mongoose");

const customerSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: [true, "First name is required"],
    },

    lastName: {
      type: String,
      required: [true, "Last name is required"],
    },

    mobileNumber: {
      type: String,
      required: [true, "Mobile number is required"],
      unique: true,
      min: 10,
      max:10
    },

    DOB: {
      type: Date,
      required: [true, "DOB is required"],
    },

    emailId: {
      type: String,
      unique: [true, "Email id already used"],
    },

    address: {
      type: String,
      required: [true, "Address is required"],
    },

    customerID: {
      type: String,
      required: [true, "customerID is required"],
    },

    status: {
      type: String,
      enum: ["ACTIVE", "INACTIVE"],
      required: [true, "status is required"],
    },

    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

const Customer = mongoose.model("Customer", customerSchema);

module.exports = Customer;
