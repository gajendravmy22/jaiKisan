const mongoose = require("mongoose");

const cardSchema = new mongoose.Schema(
  {
    cardNumber: {
      type: String,
      required: [true, "card Number is required"],
    },

    cardType: {
      type: String,
      enum: ["REGULAR", "SPECIAL"],
      required: [true, "Card type is required"]
    },

    customerName: {
      type: String,
      required: [true, "customer name is required"],
    },

    status: {
      type: String,
      enum: ["ACTIVE", "INACTIVE"],
      default: "ACTIVE",
    },

    vision: String,

    customerID: {
      type: String,
      ref: "Customer",
      required: [true, "customer id is required"],
    },
  },
  { timestamps: true }
);

const Card = mongoose.model("Card", cardSchema);

module.exports = Card;
