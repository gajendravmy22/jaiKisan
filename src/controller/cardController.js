const cardModel = require("../model/cardModel");
const customerModel = require("../model/customerModel");

const createCard = async (req, res) => {
  try {
    const { body } = req;

    const cards = await cardModel.find();

    // card number incrementing-----> 
    if (cards.length === 0) body["cardNumber"] = "C001";
    else {
      let prevId = cards[cards.length - 1]["cardNumber"].split("");
      prevId = prevId.splice(1).join("");

      let ID = String(Number(prevId) + 1);
      if (ID.length < prevId.length) {
        let iLen = prevId.length - ID.length;
        let zero = "0".repeat(iLen);
        ID = zero + ID;
      }

      body["cardNumber"] = "C" + ID;
    }

    const { customerID } = body;
    //find user by customerID for user exist or not 
    const user = await customerModel.findOne({ customerID: customerID }); 

    // find card by customerID for checking if user card already created or not
    const card = await cardModel.findOne({ customerID: customerID }); 
    if (!user)
      return res.status(404).send({
        status: "Invalid!",
        message: "No USER found with this Customer Id",
      });

    if (card)
      return res
        .status(400)
        .send({ status: "FAILED", message: "card already created" });

    body["customerName"] = `${user.firstName} ${user.lastName}`;
    const createCard = await cardModel.create(body);

    res.status(200).send({ status: "success", data: createCard });
  } catch (err) {
    res.status(500).send({ status: "ERROR", message: err.message });
  }
};

const ListCard = async (req, res) => {
  try {
    const cardList = await cardModel.find();
    res.status(200).send({status: "SUCCESS", result: cardList.length, data: cardList})
  } catch (err) {
    res.status(500).send({ status: "ERROR", message: err.message });
  }
};

module.exports = { createCard, ListCard };
