const customerModel = require("../model/customerModel");

const createCustomer = async (req, res) => {
  try {
    const { body } = req;

    body["customerID"] = Date.now(); //creating UUID
    const { mobileNumber } = body;

    //mobile number validation----> 
    if (mobileNumber.length !== 10 || Number(mobileNumber.split("")[0]) < 6)
      return res.status(400).send({
        status: "INVALID!",
        message: "Enter Valid mobile Number!",
      });

    const data = body;
    const createData = await customerModel.create(data);

    res.status(200).send({ status: "SUCCESS", data: createData });
  } catch (err) {
    res.status(500).send({ status: "ERROR", message: err.message });
  }
};

const getAllCustomer = async (req, res) => {
  try {
    const find = await customerModel.find({
      status: "ACTIVE",
      isDeleted: false,
    });

    res.status(400).send({ status: "data successfully fetched", data: find });
  } catch (err) {
    res.status(500).send({ status: "ERROR", message: err.message });
  }
};

const deleteCustomer = async (req, res) => {
  try {
    const { params } = req;
    const { id } = params;

    // const findUser = await customerModel.findOne({ customerID: id });

    let deleteUSER = await customerModel.findOneAndUpdate(
      { customerID: id, isDeleted: false },
      { isDeleted: true }
    );
    if (!deleteUSER)
      return res
        .status(404)
        .send({ status: "INVALID ID", message: "USER NOT FOUND WITH THIS ID" });

    res.status(200).send({ status: "SUCCESS", message: "USER DELETED" });
  } catch (err) {
    res.status(500).send({ status: "ERROR", message: err.message });
  }
};

module.exports = { createCustomer, getAllCustomer, deleteCustomer };
